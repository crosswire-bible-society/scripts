# scripts

This repository contains divers scripts to help conversion from usfm to osis:
 - `ref2osis.awk` converts the ref in an osis file to osis ref.
 - `Ref2Osis.sh` loop for `ref2osis.awk`
 - `note2txt.sh` merges a file with notes in an usfm file.
 - `loop4note2text.sh` loop for the `note2txt.sh` script.
 - `parenthesis_to_ref.py` is a python script to replace Perenthesis reference with inline `<ref></ref>`

## Guide for `parenthesis_to_ref.py`

This script is used to replace references in parentheses with inline `<ref></ref>` tags in a text file.

### Prerequisites

The script requires `python3` to run. You can check if you have `python3` installed on your system by running the following command in your terminal:

```bash
python3 --version
```

If `python3` is installed, this command will display the version that you have. If it's not installed, you can install it using the following command (on Debian/Ubuntu):

```bash
sudo apt install python3 python3-dev
```

### Running the script

To run the script, you need to provide the source file (the file with the references in parentheses) and the output file (the file where the script will write the modified content) as command line arguments.

Here's the general syntax for running the script:

```bash
python3 parenthesis_to_ref.py -s source.txt -o output.txt
```

Replace `source.txt` with the path to your source file and `output.txt` with the path to your output file.

### Example

Let's say you have a source file named `example.txt` with the following content:

```
Chapter 1
1. This is some text (1).
2. This is some more text (2).
3. This is even more text (3).

(1) My first reference
(2) My second reference
(3) My third reference
```

You want to replace the references in parentheses with inline `<ref></ref>` tags and write the result to a new file named `output.txt`.

You can do this by running the `parenthesis_to_ref.py` script with the `-s` option for the source file and the `-o` option for the output file:

```bash
python3 parenthesis_to_ref.py -s example.txt -o output.txt
```

After running this command, the `output.txt` file will have the following content:

```
Chapter 1
1. This is some text <ref>My first reference</ref>.
2. This is some more text <ref>My second reference</ref>.
3. This is even more text <ref>My third reference</ref>.
```