#!/bin/bash
#Créé par Kamaris membre du forum Ubuntu Le 03/12/2019 (https://forum.ubuntu-fr.org/profile.php?id=1716233) post (https://forum.ubuntu-fr.org/viewtopic.php?id=2019549&p=2)



awk_args=();
for f in "$@"; do
  if ! [ -f "$f" ]; then
    echo "Veuillez donner des fichiers d'entrée valides en arguments du script." >&2
    exit 1
  fi

  if [[ $(basename "$f") =~ [^.]+\.[^.]+$ ]]; then
    fnotes="${f%.*}_notes.${f##*.}"
  else
    fnotes="${f}_notes"
  fi

  if ! [ -f "$fnotes" ]; then
    echo "Le fichier de notes $fnotes n'a pas été trouvé." >&2
    exit 1
  fi
  awk_args+=("$fnotes" "$f")
done

awk '
# Définition des motifs d’expressions régulières pour le repérage des notes
BEGIN{
  cvpattern1="[[:blank:]]*[0-9]+[[:blank:]]*,[[:blank:]]*[0-9]+[a-z]?[[:blank:]]*"
  cvpattern2="([[:blank:]]*[0-9]+[[:blank:]]*,)?[[:blank:]]*[0-9]+[a-z]?[[:blank:]]*"
  cvpattern="^" cvpattern1 "(-" cvpattern2 ")?(\\.)?[[:blank:]]+"
  vpattern1="[[:blank:]]*[0-9]+[a-z]?[[:blank:]]*(-" cvpattern2 ")?\\."
  vpattern="^" vpattern1 "[[:blank:]]+"
  excluded_tags="\\\\(add|ca|cp|fig|it|nd|qt|va|vp|x)"
}

# Fichier de notes passé en premier : on construit le tableau des notes formatées
(FILENAME ~ ".*_notes.*"){
  if (FNR == 1){
    # Génération du fichier *_notes*.unused pour le précédent fichier de notes
    if (fnotes_name != ""){
      printf "" > fnotes_name ".unused"
      for (i=1;i<=cmax_notes;i++){
        cchange=1
        for (j=1;j<=vmax_notes;j++){
          if (notes[i,j]){
            if (cchange == 1 && notes[i,j] ~ "^\\\\f \\+ \\\\fr" vpattern1)
              print "\\c "i >> fnotes_name ".unused"
            gsub("\\\\fp? \\+ \\\\fr | \\\\ft","",notes[i,j])
            print notes[i,j] >> fnotes_name ".unused"
            cchange=0
          }
        }
      }
    }
    # Sauvegarde du nom du fichier de notes pour la génération du futur *_notes*.unused
    fnotes_name=FILENAME
    # Réinitialisation du tableau des notes
    delete notes
  }
  # Nouvelle note du type chapitre, verset ou du type verset
  if ($0 ~ cvpattern || $0 ~ vpattern){
    format_error=0
    old_c=c; old_v=v; v=$0;
    if ($0 ~ cvpattern){
      c=$0; sub(",.*","",c); c=int(c)
      sub("^[[:blank:]]*[0-9]+[[:blank:]]*,","",v)
      match($0,cvpattern)
    }
    else match($0,vpattern)
    sub("[^0-9[:blank:]].*","",v); v=int(v)
    if (v > vmax_notes) vmax_notes=v
    if (c != old_c || v != old_v) notes[c,v]="\\f + \\fr "
    else notes[c,v]=notes[c,v] "\n\\fp + \\fr "
    note_num=substr($0,1,RLENGTH)
    notes[c,v]=notes[c,v] note_num "\\ft "
    notes[c,v]=notes[c,v] substr($0,RLENGTH+1)
    next
  }
  # Nouvelle ligne d’une même note
  if (format_error == 0 && FNR > 1){
    notes[c,v]=notes[c,v] "\n" $0
    next
  }
  # Éventuel problème de format de note
  if (format_error == 0){
    format_error=1
    print "Une erreur de format de note a été rencontrée dans le fichier " \
      FILENAME > "/dev/stderr"
    next
  }
}

# Fichier de corps de texte passé en second : insertion des notes formatées
(FNR == 1){
  printf "" > FILENAME ".cat"
  cmax_notes=c
  if (v > vmax_notes) vmax_notes=v
}
/^\\c[[:blank:]]+/{ c=$0; sub("^\\\\c","",c); sub("[^0-9[:blank:]].*","",c); c=int(c) }
/^\\v[[:blank:]]+/{ v=$0; sub("^\\\\v","",v); sub("[^0-9[:blank:]].*","",v); v=int(v) }
/\*/{
  n=split($0,a,"\\*")
  for (i=1;i<n;i++){
    if (a[i] !~ excluded_tags "$"){
      if (notes[c,v]){
        sub("\\n+$","",notes[c,v])
        printf("%s%s%s",a[i],notes[c,v],"\\f*") >> FILENAME ".cat"
        notes[c,v]=""
      }
      else printf("%s%s",a[i],"\\f + \\fr \\ft \\f*") >> FILENAME ".cat"
    }
    else printf("%s%s",a[i],"*") >> FILENAME ".cat"
  }
  printf("%s\n",a[i]) >> FILENAME ".cat"
  next
}
{ print >> FILENAME ".cat" }

# Génération du fichier *_notes*.unused pour le dernier fichier de notes
END{
  printf "" > fnotes_name ".unused"
  for (i=1;i<=cmax_notes;i++){
    cchange=1
    for (j=1;j<=vmax_notes;j++){
      if (notes[i,j]){
        if (cchange == 1 && notes[i,j] ~ "^\\\\f \\+ \\\\fr" vpattern1)
          print "\\c "i >> fnotes_name ".unused"
        gsub("\\\\fp? \\+ \\\\fr | \\\\ft","",notes[i,j])
        print notes[i,j] >> fnotes_name ".unused"
        cchange=0
      }
    }
  }
}
' "${awk_args[@]}"
